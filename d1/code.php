<?php

//Odjects as Variables

$buildingObj =(object)[
    'name' => 'Caswynn Building',
    'floors' => 8,
    'address' => (object)[
        'street' => 'Timog Ave.',
        'city' => 'Quezon City',
        'country' => 'Philippines'
    ]
];

//Objects from Classes

class Building{
    //properties
    public $name;
    public $floors;
    public $address;

    //methods
    // function setName($name){
    //     $this->name = $name;
    // }



    function getName($name){
        return "The name of the building is $this->name";
    }
}

//create a new object using the Building class

$bldgOne = new Building();
//call bldgOne's setName method to give it a name
$bldgOne->setName("Caswynn Building");